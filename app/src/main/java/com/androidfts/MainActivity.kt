package com.androidfts

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.androidfts.db.DbHelper
import com.facebook.stetho.Stetho
import com.jakewharton.rxbinding2.widget.RxTextView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {

    private lateinit var dbHelper: DbHelper
    private val movieAdapter: MovieAdapter = MovieAdapter(mutableListOf())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Stetho.initializeWithDefaults(this)

        val recyclerView = findViewById<RecyclerView>(R.id.recycler_view)
        recyclerView.adapter = movieAdapter
        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        dbHelper = DbHelper(applicationContext)
        dbHelper.populate(applicationContext)

        RxTextView.afterTextChangeEvents(findViewById(R.id.edit_text))
                .debounce(500, TimeUnit.MILLISECONDS)
                .map { it.editable().toString() }
                .filter { it.isNotEmpty() && it.length > 2 }
                .flatMap { Observable.just(dbHelper.search(it)) }
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { movieAdapter.updateMovies(it) }

    }
}