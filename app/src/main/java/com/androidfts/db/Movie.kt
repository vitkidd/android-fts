package com.androidfts.db

data class Movie(val id: Int, val title: String, val overview: String)